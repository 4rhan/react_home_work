import React, { Component } from 'react';


class Modal extends Component {
    constructor(props){
        super(props)
    }
    
         
        
    

    prevDef (e) {
        e.preventDefault()
        e.stopPropagation()
    }

    render() {

        return (
            <div>
                <div className="modal-overlay" onClick = {this.props.modalClose}>

                    <div className="modal-window"
                    style={this.props.modalColor}
                    onClick = {this.prevDef}>

                        <div className = "modalHeader">
                            <h3>{this.props.headerText}</h3>
                            {this.props.closeBtn ?
                            <p onClick = {this.props.modalClose} className = "x-close">X</p> 
                            : null} 
                        </div>

                        <p>{this.props.modalText}</p>

                        <div className = "actionsBtnConteiner" >
                            {this.props.actions}
                        </div>
                         

                    </div>

                </div>
            </div>
        );
    }
}

export default Modal;